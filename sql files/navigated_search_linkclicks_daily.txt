create or replace table DIGITAL_RETAIL_ANALYSIS.DWADMIN.DR_SF_LINK_VISITOR_DAY_PRP_TABLE as (

with calendar as (
    select distinct FISCAL_DATE, DATE_KEY--, FISCAL_WEEK_START_DATE
    from FINANCE.FINANCE.REI_CALENDAR
    where FISCAL_DATE between (select FISCAL_YEAR_START_DATE_LLY from FINANCE.FINANCE.REI_CALENDAR where FISCAL_DATE = current_date() -1) and current_date() - 1
--    where FISCAL_DATE = '2022-06-05'
)

--- unique list of nav_cat_types for search page
/*, navigation_category_type as (
    select distinct
        a.PAGE_URL
        , first_value(POST_PROP63) over (partition by PAGE_URL order by DATE_TIME desc) as LATEST_NAV_CAT_TYPE
    from CLICKSTREAM.CLICKSTREAM.HIT_DATA a
        join calendar b on (a.DATE_KEY=b.DATE_KEY)
    where 1=1
        and POST_PAGENAME='rei:nav_search:sleeping-bags'  
        and a.POST_PAGENAME NOT LIKE '//%'
        and startswith(POST_PAGENAME,'rei:nav_search') --nav searches only
        and EXCLUDE_HIT<= 0
        and HIT_SOURCE not in (5,7,8,9)
        and USERNAME = 'reiprod'     
)*/

--show me the unique page_urls and navcattype for each page engaged with on NavSearch pages, for each session. (MATCH THIS INTO SESSION_PAGEVIEW TABLE)
, page_url as (
SELECT distinct 
    HASH(SESSION_ID||'+'||VISIT_START_TIME_GMT||'+'||NVL(USERNAME,'')) as SESSION_KEY
    ,VISIT_PAGE_NUM
    ,a.POST_PROP72
    ,first_value(POST_PROP63) over (partition by POST_PROP72 order by DATE_TIME desc) as POST_PROP63
FROM CLICKSTREAM.CLICKSTREAM.HIT_DATA a
    join calendar b on (a.DATE_KEY=b.DATE_KEY)
--    join navigation_category_type c on (a.PAGE_URL=c.PAGE_URL)
  --  join DIGITAL_RETAIL_ANALYSIS.DWADMIN.DR_NAV_CAT_TYPE nc on a.POST_PROP63 = nc.NAVIGATION_CATEGORY_TYPE
where 1=1
  --  and POST_PAGENAME='rei:nav_search:sleeping-bags'
    and startswith(POST_PAGENAME,'rei:nav_search')
    and a.POST_PAGENAME NOT LIKE '//%'
    --and TRY_HEX_DECODE_STRING(HEX_ENCODE(a.PAGE_NAME)) IS NOT NULL
    and EXCLUDE_HIT<= 0
    and HIT_SOURCE not in (5,7,8,9)
    and USERNAME = 'reiprod'
)

--show me the unique links engaged and the associated PageURL on link hits for NavSearch pages for each session.
, link_url2 as (
SELECT distinct DATE_TIME
    ,HASH(SESSION_ID||'+'||VISIT_START_TIME_GMT||'+'||NVL(USERNAME,'')) as SESSION_KEY
    ,VISIT_PAGE_NUM
    ,a.POST_PROP72
    ,POST_EVAR39
    ,POST_EVAR38
    ,c.POST_PROP63
--    ,LATEST_NAV_CAT_TYPE as POST_PROP63
FROM CLICKSTREAM.CLICKSTREAM.HIT_DATA a
    join calendar b on (a.DATE_KEY=b.DATE_KEY)
    join (select distinct POST_PROP72, POST_PROP63 from page_url) c ON a.POST_PROP72 = c.POST_PROP72
--    JOIN navigation_category_type c on a.PAGE_URL=c.PAGE_URL
where 1=1
--    and post_evar39='rei:nav_search:sleeping-bags'
    and a.POST_EVAR39 NOT LIKE '//%'
    and startswith(POST_EVAR39,'rei:nav_search') --OR startswith(POST_PAGENAME,'rei:nav_search')
--    AND HASH(SESSION_ID||'+'||VISIT_START_TIME_GMT||'+'||NVL(USERNAME,'')) = 9143160252879463809
   -- AND a.DATE_KEY>20210101
    and EXCLUDE_HIT<= 0
    and HIT_SOURCE not in (5,7,8,9)
    and USERNAME = 'reiprod'
    and a.POST_EVAR38 not like '%_back button'
    and (a.POST_EVAR38 IN ('rei:results_tab:articles','rei:search_select_store_Select Store','rei:search_facets_Find in Store_stores returned','rei:searchbox_text_autosuggest','rei_nav:searchbutton')
   --             or startswith(a.SESSION_LINK_CLICK_NAME,'ct_')
        OR startswith(a.POST_EVAR38,'promo-message-banner:message')
     --           OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:destinations_')
      --          OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:activities_')
       --         OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:speciality_')
       --         OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:dates_')
        --        OR A.SESSION_LINK_CLICK_NAME ='adventures_nav:deals & special offers'
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:faqs & resources_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:locations_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:activities_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:faqs & resources_')
                -- core navigation
        OR endswith(a.POST_EVAR38,':select') -- facet engagement only
                OR startswith(a.POST_EVAR38,'rei:search_tiles_') -- tile click only
                OR startswith(a.POST_EVAR38,'rei:search_breadcrumb')
                OR startswith(a.POST_EVAR38,'rei:search_sort')
                OR startswith(a.POST_EVAR38,'rei:search_related search')
                OR startswith(a.POST_EVAR38,'rei:search_facets_Stores:')
                OR startswith(a.POST_EVAR38,'rei:search_compare')
                OR startswith(a.POST_EVAR38,'rei:banner-')
                OR startswith(a.POST_EVAR38,'rei:search_visualfacets')
                OR startswith(A.POST_EVAR38,'rei_nav:camp & hike_')
                OR startswith(A.POST_EVAR38,'rei_nav:climb_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:cycle_')
                OR startswith(A.POST_EVAR38,'rei_nav:camp & hike_')
                OR startswith(A.POST_EVAR38,'rei_nav:climb_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:water_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:run_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:fitness_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:snow_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:men_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:women_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:kids_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:deals_')
                OR startswith(A.POST_EVAR38,'rei_nav:rei_nav:brands_')
    --and POST_EVAR38 is not null --!= ''
    )
)

 
-- merged link_url CTE with navigational_category_type (unique list of NavCatTypes) and match on PageURL.... (MATCH THIS FINEL CTE INTO SESSION_LINK CLICK TABLE)
/*,link_url2 as (
SELECT -- DATE_TIME
    distinct A.SESSION_KEY
    ,A.VISIT_PAGE_NUM
    ,A.POST_EVAR72
    ,POST_EVAR39
    ,POST_EVAR38
    ,coalesce(b.POST_PROP63,'na') AS POST_PROP63
    --,coalesce(b.NAV_CAT_TYPE,'na') AS POST_PROP63
 --   ,count(distinct a.session_key)
FROM link_url a
    LEFT JOIN navigation_category_type b on b.POST_EVAR72=a.POST_EVAR72
    --LEFT JOIN DIGITAL_RETAIL_ANALYSIS.DWADMIN.DR_URL_NAV_CAT_XREF b on a.POST_EVAR72=b.PAGE_FULL_URL
--    where post_evar39 ='rei:nav_search:tote-bags'
--WHERE A.session_key = 9143160252879463809
--group by 1,2,3,4,5
)*/ 

-- impressions.
,visitors_page_level as (
    select distinct a.SESSION_PAGEVIEW_DATE as FISCAL_DATE
        ,A.page_name
        ,a.SESSION_KEY
        ,a.clickstream_visit_page_num
        ,n.POST_PROP63 AS NAVIGATION_CATEGORY_TYPE
   --     ,coalesce(n.NAV_CAT_TYPE,'na') AS NAVIGATION_CATEGORY_TYPE
    --    ,count(a.session_key)
     from clickstream.clickstream.session_pageview a
         join calendar b on (a.session_pageview_date=b.fiscal_date)
         join page_url n on (a.session_key=n.session_key and a.clickstream_visit_page_num=n.visit_page_num)
        -- left join navigation_category_type n on (n.POST_EVAR72=a.PAGE_URL)
         --LEFT JOIN DIGITAL_RETAIL_ANALYSIS.DWADMIN.DR_URL_NAV_CAT_XREF n on n.PAGE_FULL_URL=a.PAGE_URL --n.VISIT_PAGE_NUM=a.CLICKSTREAM_VISIT_PAGE_NUM AND a.SESSION_KEY=n.SESSION_KEY
     where 1=1
  --         and a.PAGE_NAME='rei:nav_search:sleeping-bags'
            and a.PAGE_NAME NOT LIKE '//%'
   --         and TRY_HEX_DECODE_STRING(HEX_ENCODE(a.PAGE_NAME)) IS NOT NULL
    --        and a.page_name = 'rei:nav_search:tote-bags'
   --       and a.session_key= 9143160252879463809
            and startswith(A.PAGE_NAME,'rei:nav_search') -- A.PAGE_TEMPLATE IN ('nav_search') --,'user_search') 
            and a.report_suite='reiprod'
 --     group by 1,2,3
            
            --and a.site_id='rei' and A.PAGE_NAME like 'rei:%'
  --    group by 1,2,3,4,5,6--,4--,5,6,7,8,9
)

--customer profile on first session
,firstsession as (
    select distinct 
    a.SESSION_START_DATE AS FISCAL_DATE
    ,a.device_type
--    ,a.visitor_key
    ,a.session_key
    -- ,ROW_NUMBER() OVER (PARTITION BY a.VISITOR_KEY, a.SESSION_START_DATE ORDER BY a.session_start_datetime) as ROWNUMBER
  -- ,first_value(session_key) over (partition by visitor_key,b.FISCAL_DATE order by session_start_datetime) as FIRST_SESSION_KEY
    ,MEMBERSHIP_STATUS as MEMBER_FLAG
 --  ,max(case when a.MEMBERSHIP_STATUS = 'MEMBER' then 1 else 0 end) over (partition by a.VISITOR_KEY, a.SESSION_START_DATE order by a.session_start_datetime) as MEMBER_FLAG
        ,CASE WHEN a.FIRST_MARKETING_PROGRAM IN ('Email - Commercial', 'Email - Commercial Garage','Email - Non-Commercial', 'Email - Transactional') THEN 'Email - Comm, Non-Comm & Transactional'
       --   WHEN a.FIRST_MARKETING_PROGRAM IN ('Referring Sites', 'Vendor') THEN 'Referring Sites and Vendor'
          WHEN a.FIRST_MARKETING_PROGRAM IN ('Display Ads', 'Retargeting','Affiliates', 'Social','Referring Sites', 'Vendor') THEN 'Display, Retargeting,  Affiliates, Social, Referring and Vendor sites'
          WHEN a.FIRST_MARKETING_PROGRAM IN ('Paid Search', 'PLA') THEN 'Paid Search and PLA'
          WHEN a.FIRST_MARKETING_PROGRAM = 'ERROR' THEN 'Unknown'
          ELSE a.FIRST_MARKETING_PROGRAM END AS CHANNEL
    ,CASE WHEN a.ENTRY_PAGE_NAME='rei:home' THEN 'rei:home'
         -- WHEN a.ENTRY_PAGE_NAME like '%stores%' THEN 'Other'
          WHEN a.ENTRY_PAGE_TEMPLATE='cat_main' THEN 'Category page'
          WHEN a.ENTRY_PAGE_TEMPLATE='brands' THEN 'Brands page'
          WHEN a.ENTRY_PAGE_NAME IN ('rei:landing page:anniversary sale','rei:landing page:co-op sale','rei:landing page:gear up get out sale','rei:landing page:cyber monday','rei:landing page:holiday deals','rei:landing page:labor day sale','rei:landing page:fourth of july sale','rei:deals','rei:product_launch:generation-e','rei:member benefits') THEN a.ENTRY_PAGE_NAME
          WHEN a.ENTRY_PAGE_TEMPLATE IN ('user_search') OR startswith(a.ENTRY_PAGE_NAME,'rei:user_search') THEN 'Query search'
          WHEN a.ENTRY_PAGE_TEMPLATE IN ('nav_search') OR startswith(a.ENTRY_PAGE_NAME,'rei:nav_search') THEN 'Navigated search'
          WHEN startswith(a.ENTRY_PAGE_NAME,'rei:product') THEN 'Product page'
          ELSE 'Other' END AS ENTRY_PAGE
    ,a.VISITOR_STATUS
    ,e.TOTAL_PDP_FLAG
    ,e.TOTAL_CART_ADD_FLAG
    ,e.ORDER_CONFIRMATION_FLAG
    ,a.SESSION_CUSTOMER_DEMAND_NET_AMT
--    , max(case when LOGIN_STATUS = 'logged in' then 1 else 0 end) 
--            over (partition by visitor_key,b.FISCAL_DATE order by session_start_datetime) as LOGIN_FLAG
    from clickstream.clickstream.session_customer a
        join (select distinct c.SESSION_KEY from visitors_page_level c) c ON (c.SESSION_KEY=a.SESSION_KEY)
        join calendar b on a.SESSION_START_DATE = b.FISCAL_DATE
        join (select distinct SESSION_KEY, TOTAL_PDP_FLAG, TOTAL_CART_ADD_FLAG, ORDER_CONFIRMATION_FLAG
              from CLICKSTREAM.CLICKSTREAM.SESSION_EVENT
              ) e on a.SESSION_KEY=e.SESSION_KEY
    where 1=1
        and report_suite='reiprod' --or report_suite='reiapp%'
        and ENTERPRISE_TRAFFIC_FLAG=1
        --and a.ENTRY_PAGE_NAME like 'rei:%'
-- order by VISITOR_KEY, fiscal_date desc limit 10000
) 

--links associated with page
,visitors_link_keymetrics2 as (
    select distinct
        c.FISCAL_DATE
        ,a.DEVICE_TYPE
        ,a.SESSION_KEY
        ,a.clickstream_visit_page_num
        ,A.REFERRING_PAGE_NAME as PAGE_NAME
        ,n.POST_PROP63 AS NAVIGATION_CATEGORY_TYPE
        ,CASE WHEN a.SESSION_LINK_CLICK_NAME like '%store%' THEN 'Find in store'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_tiles_') THEN 'Product tile'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_related search') THEN 'Related search'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_breadcrumb') THEN 'Breadcrumb'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_sort') THEN 'Sort by'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_compare') THEN 'Compare tool'
              WHEN endswith(a.SESSION_LINK_CLICK_NAME,':select') THEN concat('f-',SUBSTR(SPLIT(a.SESSION_LINK_CLICK_NAME, ':')[1],15)) -- FACET HEADER NAME
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_visualfacets') THEN 'Visual filter'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:banner-') THEN 'Search banners'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'promo') THEN 'Promo banner'
              WHEN startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav') and a.session_link_click_name <> 'rei_nav:searchbutton' THEN 'Mega menu'
          --    WHEN A.SESSION_LINK_CLICK_NAME like '%footer%' THEN 'Footer'
         --     WHEN startswith(A.SESSION_LINK_CLICK_NAME,'universal_nav') THEN 'Unav'
              WHEN A.SESSION_LINK_CLICK_NAME IN ('rei:searchbox_text_autosuggest','rei_nav:searchbutton') THEN 'Query search'
              WHEN A.SESSION_LINK_CLICK_NAME IN ('rei:results_tab:articles') THEN 'Articles tab'
              ELSE 'Other' END AS LINK_CLICK_GROUP_NAME
            ,CASE WHEN startswith(LINK_CLICK_GROUP_NAME,'f-') THEN 'Facet'
                 ELSE LINK_CLICK_GROUP_NAME END AS LINK_CLICK_PARENT_GROUP_NAME 
           ,CASE WHEN a.SESSION_LINK_CLICK_NAME like '%store%' THEN 'Find in store'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_tiles_') THEN 'Product tile'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_related search') THEN 'Related search'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_breadcrumb') THEN 'Breadcrumb'
              WHEN startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_compare') THEN 'Compare tool'
              WHEN A.SESSION_LINK_CLICK_NAME IN ('rei:searchbox_text_autosuggest','rei_nav:searchbutton') THEN 'Query search'
              WHEN A.SESSION_LINK_CLICK_NAME IN ('rei:results_tab:articles') THEN 'Articles tab'
              WHEN endswith(a.SESSION_LINK_CLICK_NAME,':select') THEN SPLIT(A.SESSION_LINK_CLICK_NAME, ':')[2]
              WHEN LINK_CLICK_GROUP_NAME = 'Mega menu' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'rei_nav:','')
            --
     --       WHEN LINK_CLICK_GROUP_NAME = 'Footer' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'footer','')
     --         WHEN LINK_CLICK_GROUP_NAME = 'Unav' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'universal_nav:','')
              WHEN LINK_CLICK_GROUP_NAME = 'Promo banner' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'promo-message-banner:','')
              WHEN LINK_CLICK_GROUP_NAME = 'Visual filter' THEN REPLACE(SESSION_LINK_CLICK_NAME,'rei:search_visualfacets_label:','') 
              WHEN LINK_CLICK_GROUP_NAME = 'Sort by' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'rei:search_sort_','')
              WHEN LINK_CLICK_GROUP_NAME = 'Search banners' THEN  REPLACE(SESSION_LINK_CLICK_NAME,'rei:banner-','')
              ELSE LINK_CLICK_GROUP_NAME END AS LINK_CLICK_NAME   
             -- ,count(distinct session_key) as click_count
              ,1 as CLICK_FLAG
    from clickstream.clickstream.session_link_click a
        join calendar c on a.session_link_click_date=c.fiscal_date
        JOIN (select distinct session_key, visit_page_num, post_prop63 from link_url2) n on (a.SESSION_KEY=n.SESSION_KEY AND n.VISIT_PAGE_NUM=a.CLICKSTREAM_VISIT_PAGE_NUM) --AND a.PAGE_NAME=n.PAGE_NAME)
    where 1=1
    --    and TRY_HEX_DECODE_STRING(HEX_ENCODE(a.SESSION_LINK_CLICK_NAME)) IS NOT NULL
   --     and a.session_key= 9143160252879463809
     --   and referring_digital_site_id='rei'
     --   and referring_page_name  like any ('rei:%') -- 
   --     and a.REFERRING_PAGE_NAME ='rei:nav_search:sleeping-bags'
   --     and a.SESSION_LINK_CLICK_NAME like '%Temperature%'
        and (a.report_suite = 'reiprod') -- OR a.report_suite like 'reiapp%') -- .com and app
        and a.SESSION_LINK_CLICK_NAME not like '%_back button'
        and startswith(REFERRING_PAGE_NAME,'rei:nav_search')  -- a.REFERRING_PAGE_TEMPLATE IN ('nav_search') --,'user_search')
        and (a.SESSION_LINK_CLICK_NAME IN ('rei:results_tab:articles','rei:search_select_store_Select Store','rei:search_facets_Find in Store_stores returned','rei:searchbox_text_autosuggest','rei_nav:searchbutton')
   --             or startswith(a.SESSION_LINK_CLICK_NAME,'ct_')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'promo-message-banner:message')
     --           OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:destinations_')
      --          OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:activities_')
       --         OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:speciality_')
       --         OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:dates_')
        --        OR A.SESSION_LINK_CLICK_NAME ='adventures_nav:deals & special offers'
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'adventures_nav:faqs & resources_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:locations_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:activities_')
        --        OR startswith(A.SESSION_LINK_CLICK_NAME,'classes & events_nav:faqs & resources_')
                -- core navigation
                OR endswith(a.SESSION_LINK_CLICK_NAME,':select') -- facet engagement only
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_tiles_') -- tile click only
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_breadcrumb')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_sort')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_related search')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_facets_Stores:')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_compare')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:banner-')
                OR startswith(a.SESSION_LINK_CLICK_NAME,'rei:search_visualfacets')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:camp & hike_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:climb_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:cycle_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:camp & hike_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:climb_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:water_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:run_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:fitness_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:snow_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:men_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:women_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:kids_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:deals_')
                OR startswith(A.SESSION_LINK_CLICK_NAME,'rei_nav:rei_nav:brands_')
  --              OR startswith(A.SESSION_LINK_CLICK_NAME,'universal_nav')
              --  OR A.SESSION_LINK_CLICK_NAME like '%footer%'
            )
      --      group by 1,2,4,5,6,7,8,9
)
          
select a.FISCAL_DATE
    ,a.CHANNEL
    ,a.ENTRY_PAGE
    ,a.MEMBER_FLAG
    ,a.VISITOR_STATUS
    ,a.DEVICE_TYPE
    ,b.PAGE_NAME
    ,'na' PAGE_TEMPLATE
 --   ,b.NAVIGATION_CATEGORY_TYPE
    ,0 as DEALS_SEARCH_PAGE
    ,coalesce(b.NAVIGATION_CATEGORY_TYPE,'na') as NAVIGATION_CATEGORY_TYPE
    ,count(distinct b.SESSION_KEY) AS PAGE_COUNT
    ,count(distinct Case when TOTAL_PDP_FLAG=1 THEN b.session_key END) AS PDP_COUNT --coalesce(sum(a.TOTAL_PDP_FLAG),0) as PDP_COUNT
    ,count(distinct Case when TOTAL_CART_ADD_FLAG=1 THEN b.session_key END) AS ATC_COUNT--coalesce(sum(a.TOTAL_CART_ADD_FLAG),0) as ATC_COUNT
    ,count(distinct Case when ORDER_CONFIRMATION_FLAG=1 THEN b.session_key END) AS ORDER_COUNT --coalesce(sum(a.ORDER_CONFIRMATION_FLAG),0) as ORDER_COUNT
    ,coalesce(sum(a.SESSION_CUSTOMER_DEMAND_NET_AMT),0) as DEMAND
    ,coalesce(c.LINK_CLICK_PARENT_GROUP_NAME,'NA') as "Link Parent Group Name"
    ,coalesce(c.LINK_CLICK_GROUP_NAME,'NA') as "Link Group Name"
    ,coalesce(c.LINK_CLICK_NAME,'NA') as "Link Name" --rei.rei.cleanstring(
   -- ,count(distinct a.VISITOR_KEY) AS PAGE_COUNT
   ,count(distinct c.session_key) AS Clicks
--   ,coalesce(sum(c.CLICK_FLAG),0) as Clicks --count(distinct b.VISITOR_KEY) AS LINKCLICK_COUNT -- (sum(b.CLICK_COUNT),0) as LINKCLICK_COUNT 
  from firstsession a
        join (select distinct session_key, page_name, navigation_category_type from visitors_page_level) b ON (a.SESSION_KEY=b.SESSION_KEY)
        left join (select distinct session_key, page_name, navigation_category_type, LINK_CLICK_PARENT_GROUP_NAME, LINK_CLICK_GROUP_NAME, LINK_CLICK_NAME from visitors_link_keymetrics2) c ON (c.SESSION_KEY=b.SESSION_KEY and c.PAGE_NAME=b.PAGE_NAME and c.NAVIGATION_CATEGORY_TYPE=b.NAVIGATION_CATEGORY_TYPE)
WHERE 1=1
--    and b.PAGE_NAME ='rei:nav_search:sleeping-bags'
 --   AND ROWNUMBER=1
--    and b.page_name = 'nav_search:tote-bags'
--    and a.session_key = 9143160252879463809
--    and TRY_HEX_DECODE_STRING(HEX_ENCODE(b.PAGE_NAME)) IS NOT NULL
--    and TRY_HEX_DECODE_STRING(HEX_ENCODE(c.LINK_CLICK_NAME)) IS NOT NULL
group by 1,2,3,4,5,6,7,8,9,10,16,17,18 -- ,11,12,13,14,15,16--,13--,13--,13--,11,12--,14,15,16--,6,7,8,9,10,11
--having PAGE_COUNT>100 --and LINKCLICK_COUNT>5
);
